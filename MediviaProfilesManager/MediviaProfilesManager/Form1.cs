﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using IWshRuntimeLibrary;
using System.IO;
using System.Management;

namespace MediviaProfilesManager
{
    public partial class Form1 : Form
    {
        static string clientPath = "";
        public Form1()
        {
            InitializeComponent();

            
            RegistryKey users = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", true);
            if (users != null)
            {
                foreach (var value in users.GetValueNames())
                {
                    if (value.Contains("medivia-"))
                    {
                        listBox1.Items.Add(value.ToString());
                    }
                }
                
                
                // Set up the delays for the ToolTip.
                toolTip1.AutoPopDelay = 5000;
                toolTip1.InitialDelay = 1000;
                toolTip1.ReshowDelay = 500;
                // Force the ToolTip text to be displayed whether or not the form is active.
                toolTip1.ShowAlways = true;

                // Set up the ToolTip text for the Button and Checkbox.
                toolTip1.SetToolTip(this.button1, "This button is used to add new profiles to launch medivia with.\n Clicking it will create new windows user with username matching profile name, and hide it from visible accounts(for example windows login screen).\nPasswords for users created by this program is always '1', if it does not work, refer to button in Fixes.");
                toolTip1.SetToolTip(this.button2, "This button is used to create a shortcut on desktop that allows you to launch medivia with specified profile.\n It will be created with profile name as shortcut's name, but you are free to change it. \nIn description of the shortcut you can find information on what profile it is supposed to start medivia with.");
                toolTip1.SetToolTip(this.button3, "This button is used to delete currently highlighted profile. \nTHIS IS IRREVERSIBLE, IF YOU DELETE A PROFILE, IT WILL DELETE WINDOWS USER WHOSE NAME IS EXACTLY SAME AS THIS PROFILE!! It means you will lose map and config of that profile.");
                toolTip1.SetToolTip(this.button4, "This button is added just in case if you can see any 'medivia-xxxxxx' usernames in windows login screen or user change UI, it will hide those users.");
                toolTip1.SetToolTip(this.button5, "This button is used to copy map file from your main medivia client to profile that is currently highlighted on the list. \nKeep in mind that in order for this to work, the medivia client started with highlighted profile needs to be off! \nAlso you have to log in on that profile at least once before map can be copied!"); ;
                toolTip1.SetToolTip(this.button7, "This button is used to copy config file from your main medivia client to profile that is currently highlighted on the list. \nKeep in mind that in order for this to work, the medivia client started with highlighted profile needs to be off! \nAlso you have to log in on that profile at least once before config can be copied!");
                toolTip1.SetToolTip(this.button6, "This button allows you to select executable file to start medivia with, click it and select executable that \nwill be used by the profile(you need to set path to executable file here and create new shortcut for the profile for new path to work).");
                toolTip1.SetToolTip(this.button8, "This button is used to copy spritepack from your main medivia client to profile that is currently highlighted on the list. \nKeep in mind that in order for this to work, the medivia client started with highlighted profile needs to be off! \nAlso you have to log in on that profile at least once before spritepack can be copied!");
                toolTip1.SetToolTip(this.listBox1, "This is the list of currently added profiles to run the game with!");
                toolTip1.SetToolTip(this.button9, "If cmd window flashes briefly but Medivia does not open, possible reason is that for some reason user used to launch the game had it's password removed - this button will set password '1' for all profiles again.");
                toolTip1.SetToolTip(this.button10, "If you get FATAL ERROR Unable to load game.wad, click this button, it will fix permissions for selected medivia folder.");
            }

            SelectQuery query = new SelectQuery("Win32_UserAccount");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            foreach (ManagementObject envVar in searcher.Get())
            {
                //MessageBox.Show(envVar["Name"].ToString(), "heh");
                if (envVar["Name"].ToString().Contains("medivia-") && !listBox1.Items.Contains(envVar["Name"].ToString()))
                {
                    listBox1.Items.Add(envVar["Name"].ToString());
                }
            }

            if (listBox1.Items.Count > 0)
            {
                listBox1.SelectedIndex = 0;
            }
            else
            {
                button3.Enabled = false;
            }

            if (System.IO.File.Exists(@"C:\Medivia\Medivia.exe"))
            {
                button6.Text = @"C:\Medivia\Medivia.exe";
            }
            else
            {
                openFileDialog1.ShowDialog();
                //MessageBox.Show(openFileDialog1.FileName);
                if (!System.IO.File.Exists(openFileDialog1.FileName))
                {
                    MessageBox.Show("You need to select medivia executable file! Run the program again.", "Error");
                    Environment.Exit(0);
                    Application.Exit();
                }
                button6.Text = openFileDialog1.FileName;
            }
            clientPath = button6.Text;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            string name = "medivia-"+ random.Next(100000,999999);
            listBox1.Items.Add(name);
            List<string> temp = new List<string>();
            foreach (string item in listBox1.Items)
            {
                temp.Add(item);
            }
            listBox1.SelectedIndex = listBox1.Items.Count-1;
            updateUsersInRegistry(temp);
            //adding actual user
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C net user \""+name+"\" \"1\" /ADD";
            process.StartInfo = startInfo;
            process.Start();
            button3.Enabled = true;
            fixPermissions();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon
            //adding profiles
            foreach (string item in listBox1.Items)
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", true);
                key.CreateSubKey("SpecialAccounts");
                key.Close();
                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts", true);
                key.CreateSubKey("UserList");
                key.Close();
                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", true);
                key.SetValue(item, 0, RegistryValueKind.DWord);
                key.Close();
            }
            
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            string minimapDir = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia\\minimap";
            string cacheDir = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia\\minimap_cache";
            if (!Directory.Exists(minimapDir))
            {
                MessageBox.Show("You have to log into the game at least once before using this button!", "ERROR!");
            }
            else
            {
                foreach (var item in Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\medivia\\minimap"))
                {

                    System.IO.File.Copy(item, minimapDir+"\\" + Path.GetFileName(item), true);
                }
                if (Directory.Exists(cacheDir))
                {
                    Directory.Delete(cacheDir, true);
                }
                

                MessageBox.Show("Maps have been successfully copied from main location to:\n" + minimapDir, "Maps copied!");
            }
            //System.IO.File.Copy(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\medivia\\minimap.otmm", Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) +"\\"+listBox1.SelectedItem+ "\\medivia\\minimap.otmm",true);
            
        }
        private void updateUsersInRegistry(List<string> names)
        {
            foreach (string item in names)
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", true);
                key.CreateSubKey("SpecialAccounts");
                key.Close();
                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts", true);
                key.CreateSubKey("UserList");
                key.Close();
                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", true);
                key.SetValue(item, 0, RegistryValueKind.DWord);
                key.Close();
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count>0)
            {
                RegistryKey users = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", true);
                if (users != null)
                {
                    foreach (var value in users.GetValueNames())
                    {
                        if (value.Contains(listBox1.SelectedItem.ToString()))
                        {
                            users.DeleteValue(value);
                        }
                    }
                }
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C net user \"" + listBox1.SelectedItem.ToString() + "\"  /DELETE";
                process.StartInfo = startInfo;
                process.Start();
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                if (listBox1.Items.Count>0)
                {
                    listBox1.SelectedIndex = 0;
                }
                else
                {
                    button3.Enabled = false;
                }
                
                
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + listBox1.SelectedItem + ".lnk");
            WshShell wsh = new WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = wsh.CreateShortcut(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\"+listBox1.SelectedItem+".lnk") as IWshRuntimeLibrary.IWshShortcut;
            shortcut.Arguments = "/user:" + listBox1.SelectedItem + " /savecred \""+ clientPath + "\"";
            shortcut.TargetPath = "C:\\Windows\\System32\\runas.exe";
            // not sure about what this is for
            shortcut.WindowStyle = 1;
            shortcut.Description = "Shortcut to launch medivia as user "+ listBox1.SelectedItem;
            shortcut.WorkingDirectory = Path.GetDirectoryName(clientPath);
           
            shortcut.IconLocation = clientPath;
            shortcut.Save();
            fixPermissions();
        }

        private void button6_DoubleClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            button6.Text = openFileDialog1.FileName;
            clientPath = button6.Text;
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            button6.Text = openFileDialog1.FileName;
            clientPath = button6.Text;
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia\\config.otml"))
            {
                System.IO.File.Copy(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\medivia\\config.otml", Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia\\config.otml", true);
                //File.Copy(Environment.SpecialFolder.UserProfile);
                MessageBox.Show("Map has been successfully copied from main location to:\n" + Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia\\config.otml", "Config copied!");
            }
            else
            {
                MessageBox.Show("You need to run game at least once before being able to use this button!", "ERROR!");
            }
            
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia"))
            {
                if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\medivia\\sprites.zip"))
                {
                    System.IO.File.Copy(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\medivia\\sprites.zip", Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia\\sprites.zip", true);
                    //File.Copy(Environment.SpecialFolder.UserProfile);
                    MessageBox.Show("Spritepack has been successfully copied from main location to:\n" + Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)) + "\\" + listBox1.SelectedItem + "\\medivia\\sprites.zip", "Spritepack copied!");
                }
                else
                {
                    MessageBox.Show("You don't have spritepack installed!", "Error!");
                }
            }
            else
            {
                MessageBox.Show("You need to run game at least once before using this button!", "Error!");
            }
            
            
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            foreach (string item in listBox1.Items)
            {
                //adding actual user
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C net user \"" + item + "\" \"1\"";
                process.StartInfo = startInfo;
                process.Start();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            fixPermissions();
            
        }
        private void fixPermissions()
        {
            foreach (string user in listBox1.Items)
            {
                foreach (var item in Directory.GetFiles(Path.GetDirectoryName(clientPath)))
                {
                    //fixing files permissions sigh
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    startInfo.FileName = "cmd.exe";
                    startInfo.Arguments = "/C icacls " + '"' + item + '"' + " /grant " + user + ":F";
                    //System.IO.File.AppendAllText("debug.log", "/C icacls " + '"' + item + '"' + " /grant " + user + ":F" + Environment.NewLine);
                    process.StartInfo = startInfo;
                    process.Start();
                }
            }
        }
    }
}
